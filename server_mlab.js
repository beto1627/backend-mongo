const express = require('express');
const body_parser = require('body-parser');
require('dotenv').config();//no almacenamos en una constante
const cors = require('cors');  // Instalar dependencia 'cors' con npm
const app = express();
const port = process.env.PORT || 3000;
const usersFile = require('./user.json');
const request_json = require('request-json');
const URL_BASE = '/techu/v2/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu7db/collections/';
const API_KEY = 'apiKey=' + process.env.API_KEY_MLAB;

app.use(cors());
app.options('*', cors());

var client;

app.listen(port, function(){
 console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operación GET con mlab(Collection)
app.get(URL_BASE + 'users',
   function(request, response){
    client = request_json.createClient(URL_MLAB);
    let field_string = 'f={"_id":0}&';
    client.get('users?' + field_string +  API_KEY,
      function(err, res, body){
          var msg = {};
          var status = 200;
          if(err){
            status = 500;
            msg = msg = {'msg' : 'ocurrio un error'};
          }else{
            msg = body;
          }
          response.status(status);
          response.send(msg);
      });
});

//Petición GET a un único usuarios mediante ID (Instancia)
app.get(URL_BASE + 'users/:id',
   function(request, response){
   let id = request.params.id;
   let query_string = `q={"id_user": ${id} }&`;
   let field_string = 'f={"_id":0}&';
   client = request_json.createClient(URL_MLAB);
   client.get('users?' +  query_string + field_string + API_KEY,
     function(err, res, body){
       var msg = {};
       var status = 200;
       if(err){
         status = 500;
         msg = {'msg' : 'ocurrio un error'};
       }else{
         if(body.length>0){
           msg = body;
         }else{
           status = 204;
           msg = {'msg' : `usuario con id ${id} no existe`}
         }
       }
       response.status(status);
       response.send(msg);
     });
});

//Petición GET a un único usuarios mediante ID (Instancia)
app.get(URL_BASE + 'users/:id/accounts',
   function(request, response){
   let id = request.params.id;
   let query_string = `q={"id_user": ${id} }&`;
   let field_string = 'f={"account": 1,"_id":0}&';
   client = request_json.createClient(URL_MLAB);
   client.get('users?' +  query_string + field_string + API_KEY,
     function(err, res, body){
       var msg = {};
       var status = 200;
       if(err){
         status = 500;
         msg = {'msg' : 'ocurrio un error'};;
       }else{
         if(body.length>0){
           msg = body[0].account;
         }else{
           status = 404;
           msg = {'msg' : 'usuario no encontrado'};
         }
       }
       response.status(status);
       response.send(msg);
     });
});

//Petición GET a un único usuarios mediante ID (Instancia)
app.get(URL_BASE + 'users/:id/accounts/v2',
   function(request, response){
   let id = request.params.id;
   let query_string = `q={"userID": ${id} }&`;
   let field_string = 'f={"_id":0}&';
   client = request_json.createClient(URL_MLAB);
   client.get('account?' +  query_string + field_string + API_KEY,
     function(err, res, body){
       let rpta = !err ? body : {'msg' : 'usuario no tiene cuentas'};
       response.send(rpta);
     });
});

//Operación GET con QUERY STRING
app.get(URL_BASE + 'usersq',
   function(request, response){
   console.log(request.query.id);
   console.log(request.query.country);
   response.send({'id2:':request.query.id, 'country2: ':request.query.country});
});

//Petición POST a users
app.post(URL_BASE + 'users',
   function(request, response){
     client = request_json.createClient(URL_MLAB);
     let count_string = 'c=true&';
     client.get('users?' + count_string + API_KEY,
       function(err, res, body){
            var newID = body.length+1;
            console.log("newID:" + newID);
            var newUser = {
              "id_user" : newID,
              "first_name" : request.body.first_name,
              "last_name" : request.body.last_name,
              "email" : request.body.email,
              "password" : request.body.password
            };
            client.post(URL_MLAB + "users?" + API_KEY, newUser ,
             function(err, res, body) {
               var msg = {};
               var status = 201;
               if(err){
                 status = 500;
                 msg = {'msg' : 'ocurrio un error'};
               }else{
                 msg = body;
               }
               response.send(msg);
           });
       });
});

//Petición PUT a users
app.put(URL_BASE + 'users/:id',
   function(request, response){
   client = request_json.createClient(URL_MLAB);
   let id = request.params.id;
   console.log(id);
   let query_string = `q={"id_user": ${id} }&`;
   client.get('users?'+ query_string + API_KEY ,
   function(err, res , body) {
     console.log(body);
       var cambio = '{"$set":' + JSON.stringify(request.body) + '}';
       client.put(URL_MLAB + 'users?q={"id_user": ' + id + '}&' + API_KEY, JSON.parse(cambio),
        function(err, res, body) {
          console.log("body:"+ JSON.stringify(body));
          response.send(body);
       });
   });
});


//Petición DELETE a users
app.delete(URL_BASE + 'users/:id',
   function(request, response){
      var id = request.params.id;
      var query_string='q={"id_user":' + id + '}&';
      var httpClient = request_json.createClient(URL_MLAB);
      httpClient.get('users?' +  query_string + API_KEY,
        function(err, res, body){
          var respuesta = body[0];
          console.log("body delete:"+ JSON.stringify(respuesta));
          httpClient.delete(URL_MLAB + "users/"+ respuesta._id.$oid +'?'+ API_KEY,
            function(error, res, body){
              var msg = {};
              if(err){
                msg = err;
              }else{
                msg = body;
              }
              response.send(msg);
          });
        });
});

//Method POST login
app.post(URL_BASE + "login",
  function (request, response){
    console.log("POST /colapi/v3/login");
    var email= request.body.email;
    var pass= request.body.password;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var queryStringpass='q={"password":' + pass + '}&';
    var httpClient = request_json.createClient(URL_MLAB);
    httpClient.get('users?'+ queryStringEmail + API_KEY,
    function(err, res, body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session={"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(URL_MLAB + '?q={"id_user": ' + respuesta.id_user + '}&' + API_KEY);
            httpClient.put('users?q={"id_user": ' + respuesta.id_user + '}&' + API_KEY, JSON.parse(login),
             function(err, res, bodyP) {
              response.send(body[0]);
            });
          }else {
            response.send({"msg":"contraseña incorrecta"});
          }
      }else{
        console.log("Email Incorrecto");
        response.send({"msg": "email Incorrecto"});
      }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function (request, response){
    console.log("POST /colapi/v3/logout");
    var email= request.body.email;
    var queryStringEmail='q={"email":"' + email + '","logged": true}&';
    var httpClient = request_json.createClient(URL_MLAB);
    httpClient.get('users?'+ queryStringEmail + API_KEY ,
    function(err, res , body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            httpClient.put('users?q={"id_user": ' + respuesta.id_user + '}&' + API_KEY, JSON.parse(logout),
             function(err, res, bodyP) {
              response.send(body[0]);
            });
      }else{
        console.log("Error en logout");
        response.send({"msg": "Error en logout"});
      }
    });
});

//Operación GET total
app.get(URL_BASE + 'total_users',
   function(request, response){
   response.status(200);
   response.send({"num_usuarios" : usersFile.length});
});
